package ui;

import library.Cone;
import library.IConstants;
import library.IceCreamBall;
import library.Ingredient;
import library.Order;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

import lists.List;
import lists.Queue;

public class MainUI extends JFrame implements IConstants{
	
	private JLabel time;
	private JLabel[] actualOrderLabels;
	private JLabel numberOrdersLabel;
	private JLabel verification;
	private BufferedImage coneImage;
	private Cone actualCone;
	private Queue<Order> orders;
	private Order actualOrder;
	private boolean orderControl;
	private boolean verified;
	private List<JButton> buttons;
	private List<Ingredient> ingredients;
	
	public MainUI(Queue<Order> pOrders, int maxBalls, List<Ingredient> pIngredients, BufferedImage pConeImage) {
		orders = pOrders;
		actualCone = new Cone();
		actualCone.setMaxBalls(maxBalls);
		buttons = new List<JButton>();
		actualOrderLabels = new JLabel[maxBalls];
		ingredients = pIngredients;
		coneImage = pConeImage;
		setLayout(null);
		setBounds(200,200,WIND_WIDTH,WIND_HEIGHT);
		setTitle(WIND_TITLE);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);		
		setVisible(true);	
		initComponents(ingredients);
		repaint();
	}
	
	public JLabel getTimeLabel() {
		return time;
	}
	
	public void paint(Graphics g) {
		super.paint(g);
		super.paintComponents(g);
		g.drawImage(coneImage, 65, 625, this);
		actualCone.paint(g, ICECREAM_XPOSITION, ICECREAM_YPOSITION, ICECREAM_MOVING_FACTOR, this);
	}
	
	public void initComponents(List<Ingredient> pIngredients) {
									// Label creation
		//Info label creation
		time = createLabel("",10,0,100,50,35);
		numberOrdersLabel = createLabel("Ordenes: 0", 1000, 0, 200, 50, 25);
		verification = createLabel("", 300, 300, 300, 50, 30);
		createLabel("Orden Actual: ",300,75,700,50,30);
		createLabel(WIND_TITLE, WIND_WIDTH/2-150, 0, 300, 50, 50);
		// Order label creation
		for(int i = 0; i< actualOrderLabels.length; i++) {
			actualOrderLabels[i] = createLabel("", ORDER_LABEL_XPOSITION, ORDER_LABEL_YPOSITION+i*ORDER_LABEL_MOVING_FACTOR, ORDER_LABEL_WIDTH,
					ORDER_LABEL_HEIGHT, ORDER_LABEL_SIZE);
		}
										//Button creation
		JButton entregarButton = createButton("Entregar", BUTTON_FOURTH_COLUMN, BUTTON_THIRD_ROW, BUTTON_WIDTH, BUTTON_HEIGHT, BUTTON_FONT_SIZE);
		entregarButton.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent e) {
				entregar();
			} 
		});
		
		JButton removeBallButton = createButton("Remover", BUTTON_THIRD_COLUMN, BUTTON_THIRD_ROW, BUTTON_WIDTH, BUTTON_HEIGHT, BUTTON_FONT_SIZE);
		removeBallButton.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent e) {
				actualCone.removeBall();
				repaint();
			} 
		});
		
		for(int i = 0, columns = 0, rows = 0; i< pIngredients.getLength(); i++, columns++) {
			if(columns == BUTTON_COLUMNS.length) {
				columns = 0;
				rows++;
			}
			Ingredient actual = pIngredients.getValue(i);
			buttons.add(createIngredientButton(actual.getFlavour(),
				actual.getIdImagen(),BUTTON_COLUMNS[columns],BUTTON_ROWS[rows],BUTTON_WIDTH,BUTTON_HEIGHT,BUTTON_FONT_SIZE));
		}
	}
	
	public void entregar() {
		buttons.scramble();
		sortButtons();
		if(actualCone.calculateMatch(actualOrder)){
			verification.setText("Bien hecho :D");
			changeActualOrder();
			updateNumberOrders();
		}else {
			verification.setText("Fallaste :(");
		}
		verified = true;
	}
	
	public void sortButtons() {
		for(int i = 0, columns = 0, rows = 0; i< buttons.getLength(); i++, columns++) {
			if(columns == BUTTON_COLUMNS.length) {
				columns = 0;
				rows++;
			}
			buttons.getValue(i).setBounds(BUTTON_COLUMNS[columns], BUTTON_ROWS[rows], BUTTON_WIDTH, BUTTON_HEIGHT);
		}
		repaint();
	}
	
	public void changeActualOrder() {
		cleanActualOrder();
		if(orders.Length() > 0) {
			actualOrder = orders.dequeue();
			List<IceCreamBall> actual = actualOrder.getIceCreamBalls();
			for(int i = 0; i<actual.getLength(); i++) {
				actualOrderLabels[i].setText(actual.getValue(i).toString());
			}
		}else {
			orderControl = false;
		}
	}
	
	public void cleanActualOrder() {
		for(int i = 0; i<actualOrderLabels.length; i++) {
			actualOrderLabels[i].setText("");
		}
	}
	
	public void updateNumberOrders() {
		if(orders.Length() == 1 && !orderControl) {
			changeActualOrder();
			orderControl = true;
		}
		numberOrdersLabel.setText("Ordenes: "+Integer.toString(orders.Length()));
	}
	
	public JLabel createLabel(String text,int x, int y, int width, int height,int fontSize) {
		JLabel label = new JLabel(text);
		label.setBounds(x,y,width,height);
		label.setFont(new Font(null,Font.PLAIN,fontSize));
		add(label);
		return label;
	}
	
	public JButton createButton(String text, int x, int y, int width, int height, int fontSize) {
		JButton button = new JButton(text);
		button.setBounds(x, y, width, height);
		button.setFont(new Font(null,Font.PLAIN,fontSize));
		add(button);
		return button;
	}
	
	public void addIngredient(String pName) {
		if(verified) {
			verification.setText("");
			verified = false;
		}
		Ingredient actual;
		for(int i = 0; i<ingredients.getLength(); i++) {
			actual = ingredients.getValue(i);
			if(actual.getIdImagen().equals(pName)) {
				if(actual.getType().equals("bola")) {
					actualCone.addBall(actual);
				}else {
					actualCone.addTopping(actual);
				}
				break;
			}
		}
		repaint();
	}
	
	public JButton createIngredientButton(String text, String name, int x, int y, int width, int height, int fontSize) {
		JButton button = createButton(text, x, y, width, height, fontSize);
		button.setName(name);
		button.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent e) {
				addIngredient(button.getName());
			} 
		});
		return button;
	}
}
