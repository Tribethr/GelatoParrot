package library;

public class Settings {
	
	private int maxBalls;
	private int timeToOrder;
	private int priorityOrderProbability;
	private int maxToppings;
	private int maxOrder;
	private int minOrder;
	
	public Settings(int maxBalls, int timeToOrder, int priorityOrderProbability, int maxToppings, int maxOrder, int minOrder) {
		this.maxBalls = maxBalls;
		this.timeToOrder = timeToOrder;
		this.priorityOrderProbability = priorityOrderProbability;
		this.maxToppings = maxToppings;
		this.maxOrder = maxOrder;
		this.minOrder = minOrder;
	}
	public int getMaxBalls() {
		return maxBalls;
	}

	public int getTimeToOrder() {
		return timeToOrder;
	}

	public int getPriorityOrderProbability() {
		return priorityOrderProbability;
	}


	public int getMaxToppings() {
		return maxToppings;
	}

	public int getMaxOrder() {
		return maxOrder;
	}

	public int getMinOrder() {
		return minOrder;
	}
	
	@Override
	public String toString() {
		return "Settings [maxBalls=" + maxBalls + ", timeToOrder=" + timeToOrder + ", priorityOrderProbability="
				+ priorityOrderProbability + ", maxToppings=" + maxToppings + ", maxOrder=" + maxOrder + ", minOrder="
				+ minOrder + "]";
	}
}
