package library;

public interface IConstants {
	public final int WIND_HEIGHT = 800;
	public final int WIND_WIDTH = 1200;
	public final String WIND_TITLE = "Gelato Parrot";
	public final int BORDER_MARGIN = 50;
	public final int ORDER_LABEL_XPOSITION = 500;
	public final int ORDER_LABEL_YPOSITION = 75;
	public final int ORDER_LABEL_HEIGHT = 50;
	public final int ORDER_LABEL_WIDTH = 700;
	public final int ORDER_LABEL_SIZE = 30;
	public final int ORDER_LABEL_MOVING_FACTOR = 35;
	public final int BUTTON_WIDTH = 200;
	public final int BUTTON_HEIGHT = 100;
	public final int BUTTON_FONT_SIZE = 25;
	public final int BUTTON_FIRST_COLUMN = 300;
	public final int BUTTON_SECOND_COLUMN = 510;
	public final int BUTTON_THIRD_COLUMN = 720;
	public final int BUTTON_FOURTH_COLUMN = 930;
	public final int BUTTON_FIRST_ROW = 400;
	public final int BUTTON_SECOND_ROW = 520;
	public final int BUTTON_THIRD_ROW = 640;
	public final int ICECREAM_XPOSITION = 35;
	public final int ICECREAM_YPOSITION = 485;
	public final int ICECREAM_MOVING_FACTOR = 100;
	public final int[] BUTTON_COLUMNS = {BUTTON_FIRST_COLUMN, BUTTON_SECOND_COLUMN, BUTTON_THIRD_COLUMN, BUTTON_FOURTH_COLUMN};
	public final int[] BUTTON_ROWS = {BUTTON_FIRST_ROW, BUTTON_SECOND_ROW, BUTTON_THIRD_ROW};
}
